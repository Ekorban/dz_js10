const tabTitle = document.querySelectorAll('.tabs-title');
const tabContent = document.querySelectorAll('.tabs-text');
console.log(tabTitle)
console.log(tabContent)

tabTitle.forEach(function (item){
    item.addEventListener("click", function () {

        let currentBtn = item;
        let tabId = currentBtn.getAttribute("data-category");
        let currentTab = document.querySelector(tabId);

        if (! currentBtn.classList.contains('active')) {

            tabTitle.forEach(function (item) {
                item.classList.remove('active');
            });

            tabContent.forEach(function (item) {
                item.classList.remove('active');
            });

            currentBtn.classList.add('active');
            currentTab.classList.add('active');
        }

    });
});
